﻿/// <reference path="templates/view-login.html" />
/// <reference path="templates/view-login.html" />
(function () {
    "use strict";

    angular.module("myapp", ["ionic", "myapp.controllers", "myapp.services"])
        .run(function ($ionicPlatform) {
            $ionicPlatform.ready(function () {
                if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
                    cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                }
                if (window.StatusBar) {
                    StatusBar.styleDefault();
                }
            });
        })
        .config(function ($stateProvider, $urlRouterProvider) {

            $stateProvider
            .state("app", {
                url: "/app",
                abstract: true,
                templateUrl: "app/templates/view-menu.html",
                controller: "appCtrl"
            })
            .state("home", {
                url: "/home",
                cache:false,
                templateUrl: "app/templates/view-home.html",
                controller: "homeCtrl"
            })
            .state("orderDetail", {
                url: "/orderDetail",
                templateUrl: "app/templates/view-order-detail.html",
                controller: "orderDetailCtrl",
                params: {
                    order: ''
                }
            })
            .state("printPreview", {
                url: "/printPreview",
                templateUrl: "app/templates/view-printpreview.html",
                controller: "printCtrl",
                params: {
                    orderDetail: ''
                }
            })
            .state("login", {
                url: "/login",
                cache:false,    
                templateUrl: "app/templates/view-login.html",
                controller: "loginCtrl"
            })
            .state("invitation", {
                url: "/invitation",
                templateUrl: "app/templates/view-invite-customer.html",
                controller: "invitationCtrl"
            })
            .state("register", {
                url: "/register",
                cache: false,
                templateUrl: "app/templates/view-register.html",
                controller: "registerCtrl"
            })
            .state("settings", {
                url: "/settings",
                templateUrl: "app/templates/view-settings.html",
                controller: "settingsCtrl"
            })
            ;
            if (localStorage['loggedIn'] === 'true') {
                console.log('logged In');
                $urlRouterProvider.otherwise("/home");
            }
            else {
                $urlRouterProvider.otherwise("/login");
            }

            //$urlRouterProvider.otherwise("/register");
        });
})();