﻿(function () {
    "use strict";

    angular.module("myapp.controllers", ['ionic', 'ngCordova'])

    .controller("appCtrl", ["$scope", function ($scope ) {

    }])
    .controller("homeCtrl", ["$scope", "$state", "$customlocalstorage", "$config", "$ionicLoading", "$http", "$ionicPopover", "$ionicPopup", "$filter", "$ionicHistory", "$popupService", function ($scope, $state, $customlocalstorage, $config, $ionicLoading, $http, $ionicPopover, $ionicPopup, $filter, $ionicHistory, $popupService) {
        $scope.refresh = function () {
            $scope.$broadcast("scroll.refreshComplete");
        };

        $ionicHistory.clearHistory();
        if (localStorage['tokenUpdated'] === undefined && !angular.isUndefined(localStorage['token'])) {
            console.log(localStorage['token']);
            $http.put('http://' + $config.IP_PORT + '/retailer/updateToken',
                {
                    "token": localStorage['token'],
                    "id": localStorage['retailerID']
                }).then(function (res) {
                    console.log(res);

                    if (res.data.status === "1") {
                        console.log('token updated');
                        localStorage['tokenUpdated'] = 'done';
                        $popupService.showAlert('Token Update Success', JSON.stringify(res));
                    }
                    else {
                    }
                }, function (err) {
                    console.log(err);
                    $popupService.showAlert('Token Update failed', JSON.stringify(err));
                    console.log('token update failed');
                });
        }
        else {
            console.log('token already updated');
            //$popupService.showAlert('Token aleardy updated', localStorage['token']);
        }

        $scope.data = {
            filterChoice: 'No Filter',
            orderChoice: 'Date descending'
        }
        $scope.orderList = [];
        $scope.loadOrderList = function () {
            $ionicLoading.show({
                template: 'Loading orders...'
            });
            $http.get('http://' + $config.IP_PORT + '/retailer/orders/' + localStorage['retailerID']).then(function (res, status) {
                console.log(res);
                if (res.data.length === 2) {
                    //$scope.orderList = res.data[1];
                    if ($scope.data.filterChoice === "No Filter") {
                        $scope.orderList = res.data[1];
                    }
                    else {
                        $scope.orderList = $filter('filter')(res.data[1], { status: $scope.data.filterChoice.toUpperCase() })
                    }
                    if ($scope.data.orderChoice == 'Date descending') {
                        $scope.orderList = $filter('orderBy')($scope.orderList, 'orderDate', 'reverse')
                    }
                    else {
                        $scope.orderList = $filter('orderBy')($scope.orderList, 'orderDate');
                    }
                }
                else {
                    console.log('orders response data seems not OK dude!');
                }
                $ionicLoading.hide();
            },
            function (err) {
                console.log(err);
                $ionicLoading.hide();
            })
        };
        $scope.loadOrderList();
        $scope.consumerDetail = '';
        $scope.onConsumerClick = function (order) {
            console.log(order);
            $http.get('http://' + $config.IP_PORT + '/consumer/id/' + order.consumer_id).then(function (res, status) {
                $scope.consumerDetail = res.data[1][0];
                console.log(res);
                $ionicPopup.alert({
                    template: '<div class="item item-body"> <img class="full-image" src="http://lorempixel.com/400/400/" /> <h3>' + $scope.consumerDetail.mail_id + ' </h3><p>' + $scope.consumerDetail.state + '</p></div>',
                    title: '<h3>' + $scope.consumerDetail.name + '</h3>'
                }).then(function () {
                    console.log('popup closed');
                });
            }, function (err) {
                console.log(err);
            })
        };
        $scope.onOrderClick = function (ord) {
            $state.go('orderDetail', { order: ord });
        };
        $scope.filterBy = function () {
            var filterPopup = $ionicPopup.show({
                template: '<ion-list><ion-radio value="No Filter" ng-model="data.filterChoice" ng-click="clicked1()">No Filter</ion-radio><ion-radio value="Delivered" ng-model="data.filterChoice" ng-click="clicked1()">Delivered</ion-radio><ion-radio value="New" ng-model="data.filterChoice" ng-click="clicked1()">New</ion-radio><ion-radio value="Processing" ng-model="data.filterChoice" ng-click="clicked1()">Processing</ion-radio></ion-list>',
                title: 'Filter by',
                scope: $scope
            });
            $scope.clicked1 = function () {
                console.log($scope.data.filterChoice);
                filterPopup.close();
                $scope.loadOrderList();
            };
        };
        $scope.orderBy = function () {
            var orderByPopup = $ionicPopup.show({
                template: '<ion-list><ion-radio value="Date descending" ng-model="data.orderChoice" ng-click="clicked2()">Date descending</ion-radio><ion-radio value="Date ascending" ng-model="data.orderChoice" ng-click="clicked2()">Date ascending</ion-radio></ion-list>',
                title: 'Order by',
                scope: $scope
            });
            $scope.clicked2 = function () {
                console.log($scope.data.orderChoice);
                orderByPopup.close();
                $scope.loadOrderList();
            };
        };
        $scope.getStyle = function (order) {
            if (order.status === "DELIVERED") {
                return "BLUE";
            }
            else if (order.status === "PROCESSING") {
                return "GREEN";
            }
            else if (new Date() - new Date(order.orderDate) > 172800000) {
                return "RED";
            }
            else {
                return "WHITE";
            }
        };
        $ionicPopover.fromTemplateUrl('my-popover.html', {
            scope: $scope
        })
        .then(function (popover) {
            $scope.popover = popover;
        });
        $scope.openPopover = function ($event) {
            $scope.popover.show($event);
            console.log("openPo-pover");
        };
        $scope.closePopover = function () {
            $scope.popover.hide();
            console.log("closePopover");
        };
        $scope.$on('$destroy', function () {
            $scope.popover.remove();
            console.log("$destroy");
        });
        $scope.$on('popover.hidden', function () {
            console.log("hidden");
        });
        $scope.$on('popover.removed', function () {
            console.log("removed");
        });
    }])
    .controller("orderDetailCtrl", ["$scope", "$state", "$customlocalstorage", "$config", "$ionicLoading", "$http", "$ionicPopover", "$stateParams", "$popupService", function ($scope, $state, $customlocalstorage, $config, $ionicLoading, $http, $ionicPopover, $stateParams, $popupService) {
        $scope.refresh = function () {
            $scope.$broadcast("scroll.refreshComplete");
        };
        $scope.orderDetail = {};
        $scope.rawOrderDetail = [];
        $scope.order = $stateParams.order;
        $scope.dataToPrint = {};
var dataToShow={};
        $scope.consumerDetail = {};
        $scope.loadOrderDetail = function () {
            if ($scope.order.status === 'NEW') {
                $http({
                    method: 'PUT',
                    url: 'http://' + $config.IP_PORT + '/order/changeStatus/' + $scope.order.id + '/PROCESSING',
                }).then(function (res) {
                    if (res.data[0].status === 'OK') {
                        console.log('order status update success');
                    }
                    else {
                        console.log('order status update failed');
                        console.log(res);
                    }
                }, function (err) {
                    console.log('order status update err');
                    console.log(err);
                });
            }

            $ionicLoading.show({
                template: 'Loading order detail...'
            });
            $http.get ('http://' + $config.IP_PORT + '/item/itemInfo/' + $scope.order.id).then(function (res, status) {
                $scope.rawOrderDetail = res.data[1];
                angular.forEach(res.data[1], function (value, index) {
                    if ($scope.orderDetail[value.subsegment_name] === undefined) {
                        $scope.orderDetail[value.subsegment_name] = [];
                        $scope.orderDetail[value.subsegment_name].push(value);
                    }
                    else {
                        $scope.orderDetail[value.subsegment_name].push(value);
                    }
                });
                console.log(res);
               //  $scope.dataToPrint.push(res);
                 $scope.dataToPrint= res;
                console.log( $scope.dataToPrint);
                var productname=$scope.dataToPrint.data[1][0];
                var prodocuId=$scope.dataToPrint.data[1][0].id;
              
               

                $ionicLoading.hide();
            },
            function (err) {
                console.log(err);
                $ionicLoading.hide();
            })
            $http.get('http://' + $config.IP_PORT + '/consumer/id/' + $scope.order.consumer_id).then(function (res, status) {
                $scope.consumerDetail = res.data[1][0];
                console.log(res);
            }, function (err) {
                console.log(err);
            })
        };
        $scope.loadOrderDetail();
        $scope.orderCompleted = function () {
            if ($scope.order.status !== 'DELIVERED') {
                $http({
                    method: 'PUT',
                    url: 'http://' + $config.IP_PORT + '/order/changeStatus/' + $scope.order.id + '/DELIVERED',
                }).then(function (res) {
                    console.log(res);
                    if (res.data[0].status === 'OK') {
                        console.log('order status update success');
                        $popupService.showAlert('Success', 'Order status updated as Delivered.');
                    }
                    else {
                        console.log('order status update failed');
                        console.log(res);
                    }
                }, function (err) {
                    console.log('order status update err');
                    console.log(err);
                });
            }
            else {
                $popupService.showAlert('Success', 'Order already Delivered.');
            }
        };
        $scope.toggleCategory = function (item) {
            if (item.showItems === undefined)
                item.showItems = true;
            else {
                item.showItems = item.showItems ? false : true;
            }
        }
        $scope.showCategory = function (item) {
            if (item.showItems === undefined) {
                item.showItems = true;
                return true;
            }
            else {
                return item.showItems;
            }
        };
        $scope.printPreviewData = function (res) {
            $state.go('printPreview', { orderDetail: $scope.rawOrderDetail });
        }

    }])
    .controller("printCtrl", ["$scope", "$state", "$customlocalstorage", "$config", "$ionicLoading", "$http", "$ionicPopover", "$stateParams", "$cordovaPrinter", function ($scope, $state, $customlocalstorage, $config, $ionicLoading, $http, $ionicPopover, $stateParams, $cordovaPrinter) {
        $scope.orderDetail = $stateParams.orderDetail;
        $scope.printPage = function () {
            var content = document.getElementById('listItem');


            if (!$cordovaPrinter.isAvailable()) {
                alert("Printing is not available on device");
            } else {

                $cordovaPrinter.print(content, "view-printpreview.html", function () {
                    alert('documents with items shown')
                });
            }
        }
        
      
        //var page = location.href;

        //cordova.plugins.printer.print(page, 'view-printpreview.html', function () {
        //    alert('printing finished or canceled')
        //});
    }])
    .controller("loginCtrl", ["$scope", "$state", "$customlocalstorage", "$config", "$http", "$ionicLoading", "$popupService", "$ionicHistory", "$cordovaSpinnerDialog", function ($scope, $state, $customlocalstorage, $config, $http, $ionicLoading, $popupService, $ionicHistory, $cordovaSpinnerDialog) {
        $scope.refresh = function () {
            $scope.$broadcast("scroll.refreshComplete");
        };

        $ionicHistory.nextViewOptions({
            disableBack: true
        });

        $scope.loginData = {
            TIN: '',
            mobileNo: ''
        }

        $scope.login = function () {
            $cordovaSpinnerDialog.show("", "Logging In...", true);

            $cordovaSpinnerDialog.hide();
            //$ionicLoading.show({
            //    template: 'Please wait...'
            //});
            $http.get('http://' + $config.IP_PORT + '/retailer/tin/' + $scope.loginData.TIN + '/mobileNo/' + $scope.loginData.phoneNo).then(function (res) {
                console.log(res);
                if (res.data.status === '1') {
                    $popupService.showAlert('Login Success', res.data.message).then(function () {
                        localStorage['retailerID'] = res.data.retailer.id;
                        $customlocalstorage.setObject('retailer', res.data);
                        localStorage['loggedIn'] = true;
                        $state.go('home');
                    });
                    $ionicLoading.hide();
                }
                else {
                    $popupService.showAlert('Login Failed', res.data.message);
                    $ionicLoading.hide();
                }
            }, function (err) {
                console.log(err);
                $popupService.showAlert('Login Failed', JSON.stringify(err));
                $ionicLoading.hide();
            });

        };
    }])
    .controller("registerCtrl", ["$scope", "$state", "$customlocalstorage", "$config", "$http", "$ionicLoading", "$popupService", "$cordovaGeolocation", "$cordovaSpinnerDialog", function ($scope, $state, $customlocalstorage, $config, $http, $ionicLoading, $popupService, $cordovaGeolocation, $cordovaSpinnerDialog) {
        console.log("registerCtrl");
        $scope.form = {
           
        };
        $scope.validateForm = function () {
            var popupContent = '';
            if ($scope.form.TIN === undefined) {
                popupContent = popupContent.concat("Enter the TIN value.");
            }
            else if ($scope.form.name === undefined || $scope.form.name.length < 3) {
                popupContent = popupContent.concat("Enter valid name with more than 3 character.");
            }
            else if ($scope.form.storename === undefined || $scope.form.storename.length < 3) {
                popupContent = popupContent.concat("Enter the storename.");
            }
            else if ($scope.form.email === undefined || !$scope.form.email.match(/^[\w|\d|\._]{4,}@[\w|\d|\._]{2,}\.[\w|\d|\._]+$/g)) {
                popupContent = popupContent.concat("Enter Correct email address.");
            }
            else if ($scope.form.mobileNo === undefined || !$scope.form.mobileNo.match(/^(\+\d{1,3})?-?\d{10}$/)) {
                popupContent = popupContent.concat("Enter correct phone number.");
            }

            if (popupContent !== '') {
                $popupService.showAlert('Validation Error<i class="icon item-icon-left"></i>', popupContent);
                return false;
            }
            else {
                return true;
            }
        }
        $scope.register = function (form) {

            if ($scope.validateForm()) {
               
                var reqObj = {
                    retailer: {
                        storename: $scope.form.storename,
                        name: $scope.form.name,
                        mailId: $scope.form.email,
                        latitude: 11.11,
                        longitude: 44.14,
                        imageName: "123.jpg",
                        tin: $scope.form.TIN,
                        storeaddress: {
                            zipCode: $scope.form.zipcode,
                            street: $scope.form.street,
                            city: {
                                id: 1
                            },
                            state: {
                                id: 17
                            }
                        }
                    },
                    retailerPhoneList: [{
                        type: "MOBILE",
                        contactType: "PRIMARY",
                        phoneNumber: $scope.form.mobileNo
                    }]
                }
                navigator.geolocation.getCurrentPosition(function (position) {
                    reObj.retailer.latitude = position.coords.latitude;
                    reObj.retailer.longitude = position.coords.longitude;
                    console.log(position);
                });
                console.log(reqObj);

                var req = {
                    method: "POST",
                    url: 'http://' + $config.IP_PORT + '/retailer/add',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: JSON.stringify(reqObj)
                }
               
                $http(req).then(function (res) {
                    $ionicLoading.hide();
                    console.log(res.data);
                    if (res.data.status == '1') {
                        localStorage['retailerID'] = res.data.id;
                        $customlocalstorage.setObject('registration', res.data.id);
                        $customlocalstorage.set('loggedIn', 'true');
                        $popupService.showAlert('Success', 'You are successfully registered!(#' + res.data.id + ')').then(function () {
                            $state.go("home");
                        });
                    }
                    else {
                        $popupService.showAlert('Fail', res.data.message);
                    }
                }, function (err) {
                    console.warn(err);
                    $ionicLoading.hide();
                });
            }
        };
    }])
    .controller("invitationCtrl", ["$scope", "$state", "$customlocalstorage", "$config", "$http", "$ionicLoading", "$popupService", function ($scope, $state, $customlocalstorage, $config, $http, $ionicLoading, $popupService) {
        $scope.refresh = function () {
            $scope.$broadcast("scroll.refreshComplete");
        };

        $scope.data = {
            phoneno: ''
        }

        $scope.sendInvitation = function () {
            if ($scope.data.phoneno.length != 10) {
                $popupService.showAlert('Invalid!', 'Please enter 10 digit mobile number.');
                return;
            }

            $ionicLoading.show({
                template: 'Inviting customer...'
            });
            $http.post('http://' + $config.IP_PORT + '/invitation/add', { mobileNo: $scope.data.phoneno, id: localStorage['retailerID'] }).success(function (res) {
                if (res.status === '1') {
                    console.log('invitation success');
                    $popupService.showAlert('Success', res.message);
                }
                else {
                    console.log('invitation failed');
                    $popupService.showAlert('Failed!', res.message);
                }
                console.log(res);
                $ionicLoading.hide();
            }).then(function (err) {
                console.log(err);
                $ionicLoading.hide();
            })
        };
    }])
    .controller("errorCtrl", ["$scope", "myappService", function ($scope, myappService) {
        //public properties that define the error message and if an error is present
        $scope.error = "";
        $scope.activeError = false;

        //function to dismiss an active error
        $scope.dismissError = function () {
            $scope.activeError = false;
        };

        //broadcast event to catch an error and display it in the error section
        $scope.$on("error", function (evt, val) {
            //set the error message and mark activeError to true
            $scope.error = val;
            $scope.activeError = true;

            //stop any waiting indicators (including scroll refreshes)
            myappService.wait(false);
            $scope.$broadcast("scroll.refreshComplete");

            //manually apply given the way this might bubble up async
            $scope.$apply();
        });
    }])
    .controller("settingsCtrl", ["$scope", "$state", "$customlocalstorage", "$http", "$popupService", function ($scope, $state, $customlocalstorage, $http, $popupService) {
        console.log('settingsCtrl');
        $scope.localStoragePair = [];

        angular.forEach(localStorage, function (v, i) {
            $scope.localStoragePair.push({ key: i, value: v });
        });

        $scope.resetData = function () {
            $scope.data = {
                confirm: false
            }

            $popupService.showConfirm("Reset Confirm", "Are you sure, You want to reset the Smart Retailer data!", $scope.data).then(function () {
                if ($scope.data.confirm)
                {
                    localStorage.removeItem('loggedIn');
                    localStorage.removeItem('retailer');
                    localStorage.removeItem('retailerID');
                    localStorage.removeItem('tokenUpdated');
                    $popupService.showAlert("Reset Done!", "All the app related data has beed removed. Including logged in details.").then(function () {
                        $state.go('login');
                    });
                }
            });
        }
    }])
    ;
})();